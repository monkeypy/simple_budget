# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     SimpleBudget.Repo.insert!(%SimpleBudget.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
categories = [
  %{name: "Phone Bill"},
  %{name: "Paycheck"},
  %{name: "Groceries"},
]

categories |> Enum.each(fn category ->
  SimpleBudget.Budgets.create_category(category)
end)

budgets = [
  %{name: "Income", budgeted: 4500.00},
  %{name: "Food", budgeted: 300.00},
  %{name: "Bills", budgeted: 185.47}
]

budgets |> Enum.each(fn budget ->
  SimpleBudget.Budgets.create_budget(budget)
end)

transactions = [
  %{amount: 123.45, type: "Expense", note: "Shopping at Harmons", category_id: 3, budget_id: 2},
  %{amount: 76.54, type: "Expense", note: "ProjectFI", category_id: 1, budget_id: 3},
  %{amount: 2345.67, type: "Income", note: "Employer", category_id: 2, budget_id: 1}
]

transactions |> Enum.each(fn transaction ->
  {:ok, transaction} = SimpleBudget.Budgets.create_transaction(transaction)
  SimpleBudgetWeb.Transaction.Helpers.update_budget_on_create(transaction)
end)
