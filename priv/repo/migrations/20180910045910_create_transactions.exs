defmodule SimpleBudget.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :amount, :float
      add :type, :string
      add :note, :string
      add :category_id, :integer
      add :budget_id, :integer

      timestamps()
    end

  end
end
