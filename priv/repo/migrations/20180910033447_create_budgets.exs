defmodule SimpleBudget.Repo.Migrations.CreateBudgets do
  use Ecto.Migration

  def change do
    create table(:budgets) do
      add :name, :string
      add :budgeted, :float
      add :spent, :float

      timestamps()
    end

    create unique_index(:budgets, [:name])
  end
end
