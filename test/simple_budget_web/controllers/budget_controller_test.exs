defmodule SimpleBudgetWeb.BudgetControllerTest do
  use SimpleBudgetWeb.ConnCase

  alias SimpleBudget.Budgets

  @create_attrs %{budgeted: 120.5, name: "some name", spent: 120.5}
  @update_attrs %{budgeted: 456.7, name: "some updated name", spent: 456.7}
  @invalid_attrs %{budgeted: nil, name: nil, spent: nil}

  def fixture(:budget) do
    {:ok, budget} = Budgets.create_budget(@create_attrs)
    budget
  end

  describe "index" do
    test "lists all budgets", %{conn: conn} do
      conn = get conn, budget_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Budgets"
    end
  end

  describe "new budget" do
    test "renders form", %{conn: conn} do
      conn = get conn, budget_path(conn, :new)
      assert html_response(conn, 200) =~ "New Budget"
    end
  end

  describe "create budget" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, budget_path(conn, :create), budget: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == budget_path(conn, :show, id)

      conn = get conn, budget_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Budget"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, budget_path(conn, :create), budget: @invalid_attrs
      assert html_response(conn, 200) =~ "New Budget"
    end
  end

  describe "edit budget" do
    setup [:create_budget]

    test "renders form for editing chosen budget", %{conn: conn, budget: budget} do
      conn = get conn, budget_path(conn, :edit, budget)
      assert html_response(conn, 200) =~ "Edit Budget"
    end
  end

  describe "update budget" do
    setup [:create_budget]

    test "redirects when data is valid", %{conn: conn, budget: budget} do
      conn = put conn, budget_path(conn, :update, budget), budget: @update_attrs
      assert redirected_to(conn) == budget_path(conn, :show, budget)

      conn = get conn, budget_path(conn, :show, budget)
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, budget: budget} do
      conn = put conn, budget_path(conn, :update, budget), budget: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Budget"
    end
  end

  describe "delete budget" do
    setup [:create_budget]

    test "deletes chosen budget", %{conn: conn, budget: budget} do
      conn = delete conn, budget_path(conn, :delete, budget)
      assert redirected_to(conn) == budget_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, budget_path(conn, :show, budget)
      end
    end
  end

  defp create_budget(_) do
    budget = fixture(:budget)
    {:ok, budget: budget}
  end
end
