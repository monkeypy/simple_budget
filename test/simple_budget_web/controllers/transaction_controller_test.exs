defmodule SimpleBudgetWeb.TransactionControllerTest do
  use SimpleBudgetWeb.ConnCase

  alias SimpleBudget.Budgets

  @create_attrs %{amount: 120.5, budget_id: 42, category_id: 42, note: "some note", type: "some type"}
  @update_attrs %{amount: 456.7, budget_id: 43, category_id: 43, note: "some updated note", type: "some updated type"}
  @invalid_attrs %{amount: nil, budget_id: nil, category_id: nil, note: nil, type: nil}


  def create_attrs do
    %{
      amount: 120.5,
      budget_id: Enum.at(Budgets.list_budgets(), 0).id,
      category_id: Enum.at(Budgets.list_categories(), 0).id,
      note: "some note",
      type: "some type"
    }
  end

  def create_attrs_with_names do
    %{
      amount: 120.5,
      budget: Enum.at(Budgets.list_budgets(), 0).name,
      category: Enum.at(Budgets.list_categories(), 0).name,
      note: "some note",
      type: "some type"
    }
  end

  def update_attrs do
    budget_id = Enum.at(Budgets.list_budgets(), 1).id
    category_id = Enum.at(Budgets.list_categories(), 1).id
    %{amount: 456.7, budget_id: budget_id, category_id: category_id, note: "some updated note", type: "some updated type"}
  end

  def invalid_attrs do
    %{amount: nil, budget_id: nil, category_id: nil, note: nil, type: nil}
  end

  def invalid_attrs_with_names do
    %{
      amount: nil,
      budget: nil,
      category: nil,
      note: nil,
      type: nil
    }
  end

  def fixture(:transaction) do
    {:ok, transaction} = Budgets.create_transaction(create_attrs())
    transaction
  end

  describe "index" do
    test "lists all transactions", %{conn: conn} do
      conn = get conn, transaction_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Transactions"
    end
  end

  describe "new transaction" do
    test "renders form", %{conn: conn} do
      conn = get conn, transaction_path(conn, :new)
      assert html_response(conn, 200) =~ "New Transaction"
    end
  end

  describe "create transaction" do
    setup [:create_related_tables]

    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, transaction_path(conn, :create), transaction: create_attrs_with_names()

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == transaction_path(conn, :show, id)

      conn = get conn, transaction_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Transaction"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, transaction_path(conn, :create), transaction: invalid_attrs_with_names()
      assert html_response(conn, 200) =~ "New Transaction"
    end
  end

  describe "edit transaction" do
    setup [:create_related_tables, :create_transaction]

    test "renders form for editing chosen transaction", %{conn: conn, transaction: transaction} do
      conn = get conn, transaction_path(conn, :edit, transaction)
      assert html_response(conn, 200) =~ "Edit Transaction"
    end
  end

  describe "update transaction" do
    setup [:create_related_tables, :create_transaction]

    test "redirects when data is valid", %{conn: conn, transaction: transaction} do
      conn = put conn, transaction_path(conn, :update, transaction), transaction: update_attrs
      assert redirected_to(conn) == transaction_path(conn, :show, transaction)

      conn = get conn, transaction_path(conn, :show, transaction)
      assert html_response(conn, 200) =~ "some updated note"
    end

    test "renders errors when data is invalid", %{conn: conn, transaction: transaction} do
      conn = put conn, transaction_path(conn, :update, transaction), transaction: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Transaction"
    end
  end

  describe "delete transaction" do
    setup [:create_related_tables, :create_transaction]

    test "deletes chosen transaction", %{conn: conn, transaction: transaction} do
      conn = delete conn, transaction_path(conn, :delete, transaction)
      assert redirected_to(conn) == transaction_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, transaction_path(conn, :show, transaction)
      end
    end
  end

  defp create_related_tables(_) do
    1..50 |> Enum.each(fn n ->
      Budgets.create_budget(%{name: "budget_#{n}", budgeted: n})
      Budgets.create_category(%{name: "category_#{n}"})
    end)
  end

  defp create_transaction(_) do
    transaction = fixture(:transaction)
    {:ok, transaction: transaction}
  end
end
