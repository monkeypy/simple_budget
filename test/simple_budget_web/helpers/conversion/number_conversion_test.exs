defmodule NumberConversionTest do
  use SimpleBudgetWeb.ConnCase

  describe "get format" do
    test "returns :to_roman_numeral for roman" do
      assert NumberConversion.get_format("roman") == :to_roman_numeral
    end
    test "returns :to_binary for binary" do
      assert NumberConversion.get_format("binary") == :to_binary
    end
    test "returns :to_dollar by default" do
      assert NumberConversion.get_format("dollar") == :to_dollar
      assert NumberConversion.get_format("apples") == :to_dollar
    end
  end

  describe "converts to dollar" do
    test "with a positive number" do
      assert NumberConversion.to_dollar(100) == "$100.00"
    end
    test "with a negative number" do
      assert NumberConversion.to_dollar(-100) == "$-100.00"
    end
    test "with a float number" do
      assert NumberConversion.to_dollar(123.45) == "$123.45"
    end
    test "with a string number" do
      assert NumberConversion.to_dollar("asdf") == :error
    end
  end

  describe "converts to binary" do
    test "with a positive number" do
      assert NumberConversion.to_binary(10) == "1010"
    end
    test "with a negative number" do
      assert NumberConversion.to_binary(-5) == "(-)101"
    end
    test "with a float number" do
      assert NumberConversion.to_binary(9.5) == "1010"
    end
    test "with a string number" do
      assert NumberConversion.to_binary("asdf") == :error
    end
  end

  describe "converts to roman numeral" do
    test "with a positive number" do
      assert NumberConversion.to_roman_numeral(1000) == "M"
    end
    test "with a negative number" do
      assert NumberConversion.to_roman_numeral(-10) == "(-)X"
    end
    test "with a float number" do
      assert NumberConversion.to_roman_numeral(49.5) == "L"
    end
    test "with a string number" do
      assert NumberConversion.to_roman_numeral("asdf") == :error
    end
    test "number 3" do
      assert NumberConversion.to_roman_numeral(3) == "III"
    end
    test "number 4" do
      assert NumberConversion.to_roman_numeral(4) == "IV"
    end
    test "number 5" do
      assert NumberConversion.to_roman_numeral(5) == "V"
    end
    test "number 7" do
      assert NumberConversion.to_roman_numeral(7) == "VII"
    end
    test "number 9" do
      assert NumberConversion.to_roman_numeral(9) == "IX"
    end
    test "number 10" do
      assert NumberConversion.to_roman_numeral(10) == "X"
    end
    test "number 11" do
      assert NumberConversion.to_roman_numeral(11) == "XI"
    end
    test "number 40" do
      assert NumberConversion.to_roman_numeral(40) == "XL"
    end
    test "number 50" do
      assert NumberConversion.to_roman_numeral(50) == "L"
    end
    test "number 75" do
      assert NumberConversion.to_roman_numeral(75) == "LXXV"
    end
    test "number 90" do
      assert NumberConversion.to_roman_numeral(90) == "XC"
    end
    test "number 201" do
      assert NumberConversion.to_roman_numeral(201) == "CCI"
    end
    test "number 400" do
      assert NumberConversion.to_roman_numeral(400) == "CD"
    end
    test "number 500" do
      assert NumberConversion.to_roman_numeral(500) == "D"
    end
    test "number 900" do
      assert NumberConversion.to_roman_numeral(900) == "CM"
    end
    test "number 1000" do
      assert NumberConversion.to_roman_numeral(1000) == "M"
    end
    test "number 5157" do
      assert NumberConversion.to_roman_numeral(5157) == "MMMMMCLVII"
    end
  end

end
