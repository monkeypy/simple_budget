defmodule SimpleBudgetWeb.Transaction.HelpersTest do
  use SimpleBudgetWeb.ConnCase

  alias SimpleBudget.Budgets
  alias SimpleBudgetWeb.Transaction.Helpers

  def create_transaction(params \\ []) do
    budget_id = Budgets.list_budgets() |> Enum.at(0) |> Helpers.get_table_id_of
    category_id = Budgets.list_categories |> Enum.at(0) |> Helpers.get_table_id_of
    defaults = [amount: 100, type: "Expense", note: "test note", budget_id: budget_id, category_id: category_id]
    params = Keyword.merge(defaults, params) |> Enum.into(%{})
    Budgets.create_transaction(params)
  end

  def ok_csv_transactions do
    [
      create_transaction(amount: 100),
      create_transaction(amount: 50),
      create_transaction(amount: 10),
      create_transaction(amount: 5)
    ]
  end

  def error_csv_transactions do
    [
      {:error, "Error Message", [123, "income", "note", "budget", "category"]},
      {:error, "Different Error Message", [321, "expense", "eton", "tegdub", "yrogetac"]}
    ]
  end

  def mixed_csv_transactions do
    ok_csv_transactions() ++ error_csv_transactions()
  end

  def expected_csv_report_msg do
    "Error Message [123, \"income\", \"note\", \"budget\", \"category\"]\nDifferent Error Message [321, \"expense\", \"eton\", \"tegdub\", \"yrogetac\"]"
  end

  describe "get budget from name" do
    setup [:create_related_tables]

    test "returns budget when name is valid" do
      assert %Budgets.Budget{} = Helpers.get_budget_from_name("test budget")
    end
    test "returns nil when name is invalid" do
      assert is_nil(Helpers.get_budget_from_name("this name does not exist"))
    end
  end

  describe "get category from name" do
    setup [:create_related_tables]

    test "returns category when name is valid" do
      assert %Budgets.Category{} = Helpers.get_category_from_name("test category")
    end
    test "returns nil when name is invalid" do
      assert is_nil(Helpers.get_category_from_name("this name does not exist"))
    end
  end

  describe "get table id of" do
    setup [:create_related_tables]

    test "when given an invalid object returns nil" do
      assert is_nil(Helpers.get_table_id_of(nil))
    end
    test "when given a valid budget object returns the id" do
      budget = Budgets.list_budgets() |> Enum.at(0)
      assert is_number(Helpers.get_table_id_of(budget))
    end
    test "when given a valid category object returns the id" do
      category = Budgets.list_categories() |> Enum.at(0)
      assert is_number(Helpers.get_table_id_of(category))
    end
  end

  describe "update csv transaction" do
    setup [:create_related_tables]

    test "with all transactions :ok" do
      Helpers.update_csv_transactions(ok_csv_transactions())
      budget = Budgets.list_budgets() |> Enum.at(0)
      assert budget.spent > 0
    end
    test "with all transactions :error" do
      Helpers.update_csv_transactions(error_csv_transactions())
      budget = Budgets.list_budgets() |> Enum.at(0)
      assert budget.spent == 0
    end
    test "with mixed return values" do
      Helpers.update_csv_transactions(mixed_csv_transactions())
      budget = Budgets.list_budgets() |> Enum.at(0)
      assert budget.spent > 0
    end
  end

  describe "report csv errors" do
    test "reports the errors" do
      assert Helpers.report_csv_errors(error_csv_transactions()) == expected_csv_report_msg()
    end
    test "does not report ok" do
      assert Helpers.report_csv_errors(mixed_csv_transactions()) == expected_csv_report_msg()
    end
  end

  describe "update budget on create" do
    setup [:create_related_tables]

    test "spent amount increases for Expense" do
      {:ok, transaction} = create_transaction(amount: 10)
      budget_before = Budgets.get_budget!(transaction.budget_id)
      Helpers.update_budget_on_create(transaction)
      budget_after = Budgets.get_budget!(transaction.budget_id)
      assert budget_before.spent < budget_after.spent
      assert budget_after.spent == 10.0
    end

    test "spent amount descreases for Income" do
      {:ok, transaction} = create_transaction(amount: 10, type: "Income")
      budget_before = Budgets.get_budget!(transaction.budget_id)
      Helpers.update_budget_on_create(transaction)
      budget_after = Budgets.get_budget!(transaction.budget_id)
      assert budget_before.spent > budget_after.spent
      assert budget_after.spent == -10.0
    end
  end

  describe "update budget on edit" do
    setup [:create_related_tables]

    test "spent amount increases when transaction amount increases and type is unchanged" do
      {:ok, original} = create_transaction(amount: 10)
      Helpers.update_budget_on_create(original)
      budget_before = Budgets.get_budget!(original.budget_id)

      {:ok, updated} = Budgets.update_transaction(original, %{amount: 20})
      Helpers.update_budget_on_edit(original, updated)
      budget_after = Budgets.get_budget!(updated.budget_id)

      assert budget_before.spent < budget_after.spent
      assert budget_before.spent == 10.0
      assert budget_after.spent == 20.0
    end

    test "spent amount decreases when transaction amount decreases and type is unchanged" do
      {:ok, original} = create_transaction(amount: 20)
      Helpers.update_budget_on_create(original)
      budget_before = Budgets.get_budget!(original.budget_id)

      {:ok, updated} = Budgets.update_transaction(original, %{amount: 10})
      Helpers.update_budget_on_edit(original, updated)
      budget_after = Budgets.get_budget!(updated.budget_id)

      assert budget_before.spent > budget_after.spent
      assert budget_before.spent == 20.0
      assert budget_after.spent == 10.0
    end

    test "spent amount increases when transaction amount is unchanged and type is changed from income to expense" do
      {:ok, original} = create_transaction(amount: 20, type: "Income")
      Helpers.update_budget_on_create(original)
      budget_before = Budgets.get_budget!(original.budget_id)

      {:ok, updated} = Budgets.update_transaction(original, %{type: "Expense"})
      Helpers.update_budget_on_edit(original, updated)
      budget_after = Budgets.get_budget!(updated.budget_id)

      assert budget_before.spent < budget_after.spent
      assert budget_before.spent == -20.0
      assert budget_after.spent == 20.0
    end

    test "spent amount decreases when transaction amount is unchanged and type is changed from expense to income" do
      {:ok, original} = create_transaction(amount: 20, type: "Expense")
      Helpers.update_budget_on_create(original)
      budget_before = Budgets.get_budget!(original.budget_id)

      {:ok, updated} = Budgets.update_transaction(original, %{type: "Income"})
      Helpers.update_budget_on_edit(original, updated)
      budget_after = Budgets.get_budget!(updated.budget_id)

      assert budget_before.spent > budget_after.spent
      assert budget_before.spent == 20.0
      assert budget_after.spent == -20.0
    end
  end

  describe "update budget on delete" do
    setup [:create_related_tables]

    test "spent amount is reduced by transaction amount when transaction type is Expense" do
      {:ok, original} = create_transaction(amount: 10)
      Helpers.update_budget_on_create(original)
      budget_before = Budgets.get_budget!(original.budget_id)

      {:ok, updated} = Budgets.delete_transaction(original)
      Helpers.update_budget_on_delete(updated)
      budget_after = Budgets.get_budget!(updated.budget_id)

      assert budget_before.spent > budget_after.spent
      assert budget_before.spent == 10.0
      assert budget_after.spent == 0.0
    end

    test "spent amount is increased by transaction amount when transaction type is Income" do
      {:ok, original} = create_transaction(amount: 10, type: "Income")
      Helpers.update_budget_on_create(original)
      budget_before = Budgets.get_budget!(original.budget_id)

      {:ok, updated} = Budgets.delete_transaction(original)
      Helpers.update_budget_on_delete(updated)
      budget_after = Budgets.get_budget!(updated.budget_id)

      assert budget_before.spent < budget_after.spent
      assert budget_before.spent == -10.0
      assert budget_after.spent == 0.0
    end
  end

  defp create_related_tables(_) do
    budget = Budgets.create_budget(%{name: "test budget", budgeted: 300})
    category = Budgets.create_category(%{name: "test category"})
    {:ok, budget: budget, category: category}
  end
end
