defmodule SimpleBudgetWeb.Transaction.CsvCommonTest do
  use SimpleBudgetWeb.ConnCase

  alias SimpleBudgetWeb.Transaction.CsvCommon

  def csv_row do
    [123.45, "Expense", "note", "category_name", "budget_name"]
  end

  describe "required values" do
    test "has correct required values" do
      expected = [:amount, :type, :note, :category, :budget] |> Enum.with_index
      assert expected = CsvCommon.required_values
    end
  end

  describe "get value for" do
    test "amount returns correct value" do
      assert CsvCommon.get_value_for(csv_row, :amount) == 123.45
    end
    test "type returns correct value" do
      assert CsvCommon.get_value_for(csv_row, :type) == "Expense"
    end
    test "note returns correct value" do
      assert CsvCommon.get_value_for(csv_row, :note) == "note"
    end
    test "category returns correct value" do
      assert CsvCommon.get_value_for(csv_row, :category) == "category_name"
    end
    test "budget returns correct value" do
      assert CsvCommon.get_value_for(csv_row, :budget) == "budget_name"
    end
  end
end
