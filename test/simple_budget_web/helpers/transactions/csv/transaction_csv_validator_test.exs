defmodule SimpleBudgetWeb.Transaction.CsvValidationTest do
  use SimpleBudgetWeb.ConnCase

  alias SimpleBudgetWeb.Transaction.CsvValidation
  alias SimpleBudget.Budgets

  describe "validate row" do
    setup [:create_related_tables]

    test "when too few columns" do
      row = [123.45, "Expense", "note", "category"]
      assert {:error, "Missing value", row} = CsvValidation.validate_row(row)
    end
    test "when invalid amount" do
      row = ["asdf", "Expense", "note", "category", "budget"]
      assert {:error, "Invalid amount", row} = CsvValidation.validate_row(row)
    end
    test "when invalid type" do
      row = [123.45, "invalid", "note", "category", "budget"]
      assert {:error, "Invalid type", row} = CsvValidation.validate_row(row)
    end
    test "when invalid note" do
      row = [123.45, "Expense", "", "category", "budget"]
      assert {:error, "Invalid note", row} = CsvValidation.validate_row(row)
    end
    test "when invalid category" do
      row = [123.45, "Expense", "note", "invalid_category", "budget"]
      assert {:error, "Invalid category", row} = CsvValidation.validate_row(row)
    end
    test "when invalid budget" do
      row = [123.45, "Expense", "note", "category", "invalid_budget"]
      assert{:error, "Invalid budget", row} = CsvValidation.validate_row(row)
    end
    test "when row is valid" do
      row = [123.45, "Expense", "note", "category", "budget"]
      assert {:ok, row} = CsvValidation.validate_row(row)
    end
  end

  defp create_related_tables(_) do
    budget = Budgets.create_budget(%{name: "budget", budgeted: 300})
    category = Budgets.create_category(%{name: "category"})
    {:ok, budget: budget, category: category}
  end
end
