defmodule SimpleBudget.BudgetsTest do
  use SimpleBudget.DataCase

  alias SimpleBudget.Budgets

  describe "budgets" do
    alias SimpleBudget.Budgets.Budget

    @valid_attrs %{budgeted: 120.5, name: "some name", spent: 120.5}
    @update_attrs %{budgeted: 456.7, name: "some updated name", spent: 456.7}
    @invalid_attrs %{budgeted: nil, name: nil, spent: nil}

    def budget_fixture(attrs \\ %{}) do
      {:ok, budget} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Budgets.create_budget()

      budget
    end

    test "list_budgets/0 returns all budgets" do
      budget = budget_fixture()
      assert Budgets.list_budgets() == [budget]
    end

    test "get_budget!/1 returns the budget with given id" do
      budget = budget_fixture()
      assert Budgets.get_budget!(budget.id) == budget
    end

    test "create_budget/1 with valid data creates a budget" do
      assert {:ok, %Budget{} = budget} = Budgets.create_budget(@valid_attrs)
      assert budget.budgeted == 120.5
      assert budget.name == "some name"
      assert budget.spent == 120.5
    end

    test "create_budget/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Budgets.create_budget(@invalid_attrs)
    end

    test "update_budget/2 with valid data updates the budget" do
      budget = budget_fixture()
      assert {:ok, budget} = Budgets.update_budget(budget, @update_attrs)
      assert %Budget{} = budget
      assert budget.budgeted == 456.7
      assert budget.name == "some updated name"
      assert budget.spent == 456.7
    end

    test "update_budget/2 with invalid data returns error changeset" do
      budget = budget_fixture()
      assert {:error, %Ecto.Changeset{}} = Budgets.update_budget(budget, @invalid_attrs)
      assert budget == Budgets.get_budget!(budget.id)
    end

    test "delete_budget/1 deletes the budget" do
      budget = budget_fixture()
      assert {:ok, %Budget{}} = Budgets.delete_budget(budget)
      assert_raise Ecto.NoResultsError, fn -> Budgets.get_budget!(budget.id) end
    end

    test "change_budget/1 returns a budget changeset" do
      budget = budget_fixture()
      assert %Ecto.Changeset{} = Budgets.change_budget(budget)
    end
  end

  describe "categories" do
    alias SimpleBudget.Budgets.Category

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def category_fixture(attrs \\ %{}) do
      {:ok, category} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Budgets.create_category()

      category
    end

    test "list_categories/0 returns all categories" do
      category = category_fixture()
      assert Budgets.list_categories() == [category]
    end

    test "get_category!/1 returns the category with given id" do
      category = category_fixture()
      assert Budgets.get_category!(category.id) == category
    end

    test "create_category/1 with valid data creates a category" do
      assert {:ok, %Category{} = category} = Budgets.create_category(@valid_attrs)
      assert category.name == "some name"
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Budgets.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      category = category_fixture()
      assert {:ok, category} = Budgets.update_category(category, @update_attrs)
      assert %Category{} = category
      assert category.name == "some updated name"
    end

    test "update_category/2 with invalid data returns error changeset" do
      category = category_fixture()
      assert {:error, %Ecto.Changeset{}} = Budgets.update_category(category, @invalid_attrs)
      assert category == Budgets.get_category!(category.id)
    end

    test "delete_category/1 deletes the category" do
      category = category_fixture()
      assert {:ok, %Category{}} = Budgets.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> Budgets.get_category!(category.id) end
    end

    test "change_category/1 returns a category changeset" do
      category = category_fixture()
      assert %Ecto.Changeset{} = Budgets.change_category(category)
    end
  end

  describe "transactions" do
    alias SimpleBudget.Budgets.Transaction

    @valid_attrs %{amount: 120.5, budget_id: 42, category_id: 42, note: "some note", type: "some type"}
    @update_attrs %{amount: 456.7, budget_id: 43, category_id: 43, note: "some updated note", type: "some updated type"}
    @invalid_attrs %{amount: nil, budget_id: nil, category_id: nil, note: nil, type: nil}

    def transaction_fixture(attrs \\ %{}) do
      {:ok, transaction} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Budgets.create_transaction()

      transaction
    end

    test "list_transactions/0 returns all transactions" do
      transaction = transaction_fixture()
      assert Budgets.list_transactions() == [transaction]
    end

    test "get_transaction!/1 returns the transaction with given id" do
      transaction = transaction_fixture()
      assert Budgets.get_transaction!(transaction.id) == transaction
    end

    test "create_transaction/1 with valid data creates a transaction" do
      assert {:ok, %Transaction{} = transaction} = Budgets.create_transaction(@valid_attrs)
      assert transaction.amount == 120.5
      assert transaction.budget_id == 42
      assert transaction.category_id == 42
      assert transaction.note == "some note"
      assert transaction.type == "some type"
    end

    test "create_transaction/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Budgets.create_transaction(@invalid_attrs)
    end

    test "update_transaction/2 with valid data updates the transaction" do
      transaction = transaction_fixture()
      assert {:ok, transaction} = Budgets.update_transaction(transaction, @update_attrs)
      assert %Transaction{} = transaction
      assert transaction.amount == 456.7
      assert transaction.budget_id == 43
      assert transaction.category_id == 43
      assert transaction.note == "some updated note"
      assert transaction.type == "some updated type"
    end

    test "update_transaction/2 with invalid data returns error changeset" do
      transaction = transaction_fixture()
      assert {:error, %Ecto.Changeset{}} = Budgets.update_transaction(transaction, @invalid_attrs)
      assert transaction == Budgets.get_transaction!(transaction.id)
    end

    test "delete_transaction/1 deletes the transaction" do
      transaction = transaction_fixture()
      assert {:ok, %Transaction{}} = Budgets.delete_transaction(transaction)
      assert_raise Ecto.NoResultsError, fn -> Budgets.get_transaction!(transaction.id) end
    end

    test "change_transaction/1 returns a transaction changeset" do
      transaction = transaction_fixture()
      assert %Ecto.Changeset{} = Budgets.change_transaction(transaction)
    end
  end
end
