defmodule SimpleBudget.Budgets.Budget do
  use Ecto.Schema
  import Ecto.Changeset


  schema "budgets" do
    field :budgeted, :float
    field :name, :string
    field :spent, :float, default: 0.0

    timestamps()
  end

  @doc false
  def changeset(budget, attrs) do
    budget
    |> cast(attrs, [:name, :budgeted, :spent])
    |> unique_constraint(:name)
    |> validate_required([:name, :budgeted, :spent])
  end
end
