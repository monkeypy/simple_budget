defmodule SimpleBudget.Budgets.Transaction do
  use Ecto.Schema
  import Ecto.Changeset


  schema "transactions" do
    field :amount, :float
    field :budget_id, :integer
    field :category_id, :integer
    field :note, :string
    field :type, :string

    timestamps()
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:amount, :type, :note, :category_id, :budget_id])
    |> validate_required([:amount, :type, :note, :category_id, :budget_id])
  end
end
