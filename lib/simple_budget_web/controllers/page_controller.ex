defmodule SimpleBudgetWeb.PageController do
  use SimpleBudgetWeb, :controller
  alias SimpleBudget.Budgets

  def index(conn, %{"format" => format}) do
    render(
      conn, "index.html",
      current_status: get_current_budget_status(),
      format: NumberConversion.get_format(format))
  end

  def index(conn, _params) do
    render(
      conn, "index.html",
      current_status: get_current_budget_status(),
      format: NumberConversion.get_format("dollar"))
  end

  defp get_current_budget_status() do
    budgets = Budgets.list_budgets()
    total_spent = (
      budgets
      |> Enum.map(fn budget ->
        budget.spent
      end)
      |> Enum.sum
    )
    total_budgeted = (
      budgets
      |> Enum.map(fn budget ->
        budget.budgeted
      end)
      |> Enum.sum
    )
    %{total_budgeted: total_budgeted, total_spent: total_spent}
  end
end
