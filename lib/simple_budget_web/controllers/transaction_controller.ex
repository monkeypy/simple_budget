defmodule SimpleBudgetWeb.TransactionController do
  use SimpleBudgetWeb, :controller

  import SimpleBudgetWeb.Transaction.Helpers

  alias SimpleBudget.Budgets
  alias SimpleBudget.Budgets.Transaction
  alias SimpleBudgetWeb.Transaction.CsvCreator

  def index(conn, %{"format" => format}) do
    transactions = Budgets.list_transactions()
    render(
      conn, "index.html", transactions: transactions,
      format: NumberConversion.get_format(format))
  end

  def index(conn, _params) do
    transactions = Budgets.list_transactions()
    render(
      conn, "index.html", transactions: transactions,
      format: NumberConversion.get_format("dollar"))
  end

  def new(conn, _params) do
    changeset = Budgets.change_transaction(%Transaction{})
    render(
      conn,
      "new.html",
      changeset: changeset,
      budgets: Budgets.list_budgets(),
      categories: Budgets.list_categories()
    )
  end

  def create(conn, %{"transaction" => transaction_params}) do
    case handle_create(transaction_params) do
      {:ok, transaction} ->
        update_budget_on_create(transaction)
        conn
        |> put_flash(:info, "Transaction created successfully.")
        |> redirect(to: transaction_path(conn, :show, transaction))
      {:csv_ok, transactions} ->
        update_csv_transactions(transactions)
        conn
        |> redirect(to: transaction_path(conn, :index))
      {:csv_error, transactions} ->
        update_csv_transactions(transactions)
        conn
        |> put_flash(:error, report_csv_errors(transactions))
        |> redirect(to: transaction_path(conn, :index))
      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:error, "Transaction failed to create")
        |> render(
          "new.html", changeset: changeset,
          budgets: Budgets.list_budgets(),
          categories: Budgets.list_categories())
    end
  end

  defp handle_create(transaction_params) do
    case transaction_params do
      %{"csv" => csv_file} ->
        CsvCreator.create_transactions(csv_file.path)
      _ ->
        create_transaction(transaction_params)
    end
  end

  defp create_transaction(transaction_params) do
    budget_id = get_table_id_of(get_budget_from_name(transaction_params["budget"]))
    category_id = get_table_id_of(get_category_from_name(transaction_params["category"]))
    new_transaction = Map.merge(transaction_params, %{"budget_id" => budget_id, "category_id" => category_id})
    Budgets.create_transaction(new_transaction)
  end

  def show(conn, %{"id" => id, "format" => format}) do
    transaction = Budgets.get_transaction!(id)
    render(
      conn, "show.html", transaction: transaction,
      format: NumberConversion.get_format(format))
  end

  def show(conn, %{"id" => id}) do
    transaction = Budgets.get_transaction!(id)
    render(
      conn, "show.html", transaction: transaction,
      format: NumberConversion.get_format("dollar"))
  end

  def edit(conn, %{"id" => id}) do
    transaction = Budgets.get_transaction!(id)
    changeset = Budgets.change_transaction(transaction)
    render(
      conn,
      "edit.html",
      transaction: transaction,
      changeset: changeset,
      budgets: Budgets.list_budgets(),
      categories: Budgets.list_categories()
    )
  end

  def update(conn, %{"id" => id, "transaction" => transaction_params}) do
    transaction = Budgets.get_transaction!(id)
    original_transaction = transaction
    updated_budget = Budgets.update_transaction(transaction, transaction_params)
    case updated_budget do
      {:ok, transaction} ->
        update_budget_on_edit(original_transaction, transaction)
        conn
        |> put_flash(:info, "Transaction updated successfully.")
        |> redirect(to: transaction_path(conn, :show, transaction))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(
          conn,
          "edit.html",
          transaction: transaction,
          changeset: changeset,
          budgets: Budgets.list_budgets(),
          categories: Budgets.list_categories())
    end
  end

  def delete(conn, %{"id" => id}) do
    transaction = Budgets.get_transaction!(id)
    {:ok, _transaction} = Budgets.delete_transaction(transaction)
    update_budget_on_delete(transaction)
    conn
    |> put_flash(:info, "Transaction deleted successfully.")
    |> redirect(to: transaction_path(conn, :index))
  end
end
