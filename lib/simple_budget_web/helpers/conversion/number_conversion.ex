defmodule NumberConversion do

  @doc """
  Returns a symbol for the relevant function based on the format
  that is passed to the url using http://url/enpoint?format=format_name
  """
  def get_format(format) do
    case format do
      "roman" ->
        :to_roman_numeral
      "binary" ->
        :to_binary
      _ ->
        :to_dollar
    end
  end

  @doc """
  Converts the passed in value to a normalized string in the format
  $123.45
  """
  def to_dollar(number) do
    cond do
      not is_number(number) ->
        :error
      true ->
        normalize_to_dollar(number) |> Money.to_string
    end
  end

  defp normalize_to_dollar(number) do
    {:ok, money} = :erlang.float_to_binary(number * 1.0, [decimals: 2])
    |> Money.parse(:USD)
    money
  end

  @doc """
  Converts the passed in value to binary. If the number is negative
  it will prepend it with a '(-)'
  """
  def to_binary(number) do
    if is_number(number) do
      convert_to_binary(number)
    else
      :error
    end
  end

  defp convert_to_binary(number) do
    rounded_number = round(number)
    number_as_binary_string = Integer.to_string(abs(rounded_number), 2)
    if rounded_number < 0 do
      "(-)" <> number_as_binary_string
    else
      number_as_binary_string
    end
  end

  @doc """
  Converts the passed in value to a Roman Numeral. If the number is 0 it is
  converted to 'nulla' (Roman Numeral representation of 0). If it is a
  negative number it is prepended with '(-)'
  """
  def to_roman_numeral(number) do
    cond do
      not is_number(number) ->
        :error
      round(number) == 0 ->
        "nulla"
      number > 0 ->
        convert_to_roman_numeral(round(number))
        number < 0 ->
        "(-)"<>convert_to_roman_numeral(abs(round(number)))
    end
  end

  defp convert_to_roman_numeral(number) do
    cond do
      div(number, 1000) > 0 ->
        create_roman_numeral(number, 1000, "M") <> convert_to_roman_numeral(rem(number, 1000))
      div(number, 900) > 0 ->
        create_roman_numeral(number, 900, "CM") <> convert_to_roman_numeral(rem(number, 900))
      div(number, 500) > 0 ->
        create_roman_numeral(number, 500, "D") <> convert_to_roman_numeral(rem(number, 500))
      div(number, 400) > 0 ->
        create_roman_numeral(number, 400, "CD") <> convert_to_roman_numeral(rem(number, 400))
      div(number, 100) > 0 ->
        create_roman_numeral(number, 100, "C") <> convert_to_roman_numeral(rem(number, 100))
      div(number, 90) > 0 ->
        create_roman_numeral(number, 90, "XC") <> convert_to_roman_numeral(rem(number, 90))
      div(number, 50) > 0 ->
        create_roman_numeral(number, 50, "L") <> convert_to_roman_numeral(rem(number, 50))
      div(number, 40) > 0 ->
        create_roman_numeral(number, 40, "XL") <> convert_to_roman_numeral(rem(number, 40))
      div(number, 10) > 0 ->
        create_roman_numeral(number, 10, "X") <> convert_to_roman_numeral(rem(number, 10))
      div(number, 9) > 0 ->
        create_roman_numeral(number, 9, "IX") <> convert_to_roman_numeral(rem(number, 9))
      div(number, 5) > 0 ->
        create_roman_numeral(number, 5, "V") <> convert_to_roman_numeral(rem(number, 5))
      div(number, 4) > 0 ->
        create_roman_numeral(number, 4, "IV") <> convert_to_roman_numeral(rem(number, 4))
      div(number, 1) > 0 ->
        create_roman_numeral(number, 1, "I") <> convert_to_roman_numeral(rem(number, 1))
      number == 0 ->
        ""
    end
  end

  defp create_roman_numeral(number, divisor, roman_numeral) do
    String.duplicate(roman_numeral, div(number, divisor))
  end
end
