defmodule SimpleBudgetWeb.Transaction.Helpers do
  alias SimpleBudget.Budgets

  @doc """
  Queries the database for budgets where name is 'budget_name'
  Returns either the first list item or nil
  """
  def get_budget_from_name(budget_name) do
    Budgets.get_budgets_by_name(budget_name)
    |> Enum.at(0)
  end

  @doc """
  Queries the database for categories where name is 'category_name'
  Returns either the first list item or nil
  """
  def get_category_from_name(category_name) do
    Budgets.get_categories_by_name(category_name)
    |> Enum.at(0)
  end


  @doc """
  Returns the id of a database struct or nil
  """
  def get_table_id_of(table_struct) do
    cond do
      is_nil(table_struct) ->
        nil
      true ->
        table_struct.id
    end
  end

  @doc """
  Updates the budget amount spent for each transaction in the
  CSV file that was successful
  """
  def update_csv_transactions(transactions) do
    transactions
    |> Enum.each(fn transaction ->
      case transaction do
        {:ok, t} ->
          update_budget_on_create(t)
        _ -> :ok
      end
    end)
  end

  @doc """
  Create a string to display any of the errors that occurred
  while creating transactions with the CSV file
  """
  def report_csv_errors(transactions) do
    transactions
    |> Enum.filter(fn transaction ->
      case transaction do
        {:error, _, _} ->
          true
        _ -> false
      end
    end)
    |> Enum.map(fn {:error, msg, values} -> "#{msg} #{inspect values}" end)
    |> Enum.join("\n")
  end

  @doc """
  Updates the budget amount spent based on the transaction
  """
  def update_budget_on_create(transaction) do
    budget = Budgets.get_budget!(transaction.budget_id)
    new_amount = if transaction.type == "Expense" do
      budget.spent + transaction.amount
    else
      budget.spent - transaction.amount
    end
    Budgets.update_budget(budget, %{spent: new_amount})
  end

  @doc """
  Updates the budget amount spent when a transaction is edited
  """
  def update_budget_on_edit(original, updated) do
    budget = Budgets.get_budget!(original.budget_id)
    new_amount = cond do
      original.type == updated.type ->
        budget.spent + (updated.amount - original.amount)
      original.type == "Income" ->
        budget.spent + (original.amount + updated.amount)
      original.type == "Expense" ->
        budget.spent + (-1 * (original.amount + updated.amount))
      true ->
        updated.amount
    end
    Budgets.update_budget(budget, %{spent: new_amount})
  end

  @doc """
  Updates the budget amount spent when a transaction is deleted
  """
  def update_budget_on_delete(transaction) do
    budget = Budgets.get_budget!(transaction.budget_id)
    new_amount = if transaction.type == "Expense" do
      budget.spent - transaction.amount
    else
      budget.spent + transaction.amount
    end
    Budgets.update_budget(budget, %{spent: new_amount})
  end
end
