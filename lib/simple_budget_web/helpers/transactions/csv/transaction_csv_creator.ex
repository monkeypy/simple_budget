defmodule SimpleBudgetWeb.Transaction.CsvCreator do
  import SimpleBudgetWeb.Transaction.Helpers
  import SimpleBudgetWeb.Transaction.CsvCommon

  alias SimpleBudgetWeb.Transaction.CsvValidation
  alias SimpleBudget.Budgets

  @doc """
  Read a CSV file and create a transaction for each row
  """
  def create_transactions(csv_file) do
    File.stream!(csv_file)
    |> CSV.decode
    |> Enum.map(fn {status, row} ->
      case status do
        :ok ->
          create_new_transaction(row)
        _ ->
          {status, "CSV decode error", row}
      end
    end)
    |> get_return_status
  end

  defp get_return_status(create_results) do
    cond do
      Enum.all?(create_results, fn transaction -> not is_error?(transaction) end) ->
        {:csv_ok, create_results}
      true ->
        {:csv_error, create_results}
    end
  end

  defp is_error?({:ok, _}), do: false
  defp is_error?({:error, _, _}), do: true

  defp create_new_transaction(row) do
    is_valid = CsvValidation.validate_row(row)
    case is_valid do
      {:ok, row} ->
        insert_new_transaction(row)
      _ ->
        is_valid
    end
  end

  defp convert_row_to_map(row) do
    %{
      "amount" => get_value_for(row, :amount),
      "type" => get_value_for(row, :type),
      "note" => get_value_for(row, :note),
      "category_id" => get_category_from_name(get_value_for(row, :category)).id,
      "budget_id" => get_budget_from_name(get_value_for(row, :budget)).id
    }
  end

  defp insert_new_transaction(row) do
    row |> convert_row_to_map
    |> Budgets.create_transaction
  end
end
