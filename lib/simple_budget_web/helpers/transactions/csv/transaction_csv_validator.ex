defmodule SimpleBudgetWeb.Transaction.CsvValidation do
  import SimpleBudgetWeb.Transaction.Helpers
  import SimpleBudgetWeb.Transaction.CsvCommon

  @doc """
  Validates that the given row from the CSV file is capable of being
  added as a transaction.

  Returns {:ok row} if valid
  Returns {:error, error_message, row} if invalid
  """
  def validate_row(row) do
    cond do
      not row_has_correct_item_count?(row) ->
        error_result("Missing value", row)
      not amount_valid?(row) ->
        error_result("Invalid amount", row)
      not type_valid?(row) ->
        error_result("Invalid type", row)
      not note_valid?(row) ->
        error_result("Invalid note", row)
      not category_exists?(row) ->
        error_result("Invalid category", row)
      not budget_exists?(row) ->
        error_result("Invalid budget", row)
      true ->
        {:ok, row}
    end
  end

  defp error_result(msg, row) do
    {:error, "#{msg}", row}
  end

  defp row_has_correct_item_count?(row) do
    length(row) == length(required_values())
  end

  defp amount_valid?(row, options \\ []) do
    defaults = [currency: :USD]
    options = Keyword.merge(defaults, options) |> Enum.into(%{})
    case Money.parse(get_value_for(row, :amount), options.currency) do
      {:ok, _} -> true
      _ -> false
    end
  end

  defp type_valid?(row) do
    String.capitalize(get_value_for(row, :type)) in ["Income", "Expense"]
  end

  defp note_valid?(row) do
    get_value_for(row, :note) not in ["", nil]
  end

  defp budget_exists?(row) do
    not is_nil(
      get_budget_from_name(get_value_for(row, :budget)))
  end

  defp category_exists?(row) do
    not is_nil(
      get_category_from_name(get_value_for(row, :category)))
  end
end
