defmodule SimpleBudgetWeb.Transaction.CsvCommon do

  @doc """
  Returns the value in a row for the given key based on the
  expected transaction format.
  """
  def get_value_for(row, key) do
    Enum.at(row, required_values() |> Keyword.get(key))
  end

  @doc """
  Returns a list of tuples corresponding to an item and its
  expected index based on the transaction format.
  """
  def required_values do
    # These values are in order of how they should appear in the CSV
    # They should remain this way
    Enum.with_index([:amount, :type, :note, :category, :budget])
  end
end
